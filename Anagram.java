package com.npci.application;

import java.util.Arrays;  
public class Anagram {  
    public static void main (String [] args) {  
        String str1="bast";  
        String str2="stob";  
    
        if (str1.length() != str2.length()) {  
            System.out.println("not anagram");  
        }  
        else {  
            char[] string1 = str1.toCharArray();  
            char[] string2 = str2.toCharArray();  
            
            Arrays.sort(string1);  
            Arrays.sort(string2);  

            if(Arrays.equals(string1, string2) == true) {  
                System.out.println("anagram");  
            }  
            else {  
                System.out.println("not anagram");  
            }  
        }  
    }  
}  
